package id.ac.poltek_kediri.informatika.uts_015019

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.SimpleAdapter
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val COLLECTION ="students"
    val F_ID = "id"
    val F_NAME = "name"
    val F_ADDRESS = "address"
    val F_PHONE = "phone"
    var docId = ""
    lateinit var db : FirebaseFirestore
    lateinit var alStudent : ArrayList<HashMap<String,Any>>
    lateinit var adapter : SimpleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        alStudent = ArrayList()

    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if(e != null) Log.d("fireStore",e.message)
            showData()
        }
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alStudent.clear()
            for(doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_NAME, doc.get(F_NAME).toString())
                hm.set(F_ADDRESS, doc.get(F_ADDRESS).toString())
                hm.set(F_PHONE, doc.get(F_PHONE).toString())
                alStudent.add(hm)
            }
            adapter = SimpleAdapter(this,alStudent,R.layout.row_data,
                arrayOf(F_ID,F_NAME,F_ADDRESS,F_PHONE),
                intArrayOf(R.id.txId, R.id.txName, R.id.txAddress, R.id.txPhone))
            lsData.adapter = adapter
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater =  menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemWaktu ->{
                val intent = Intent(this,LoginActivity::class.java)
                startActivity(intent)
                true
            }
            R.id.itemLink ->{
                val intent = Intent(this,MapsActivity::class.java)
                startActivity(intent)
                true
            }

            R.id.itemAbout ->{
                val intent = Intent(this,AboutActivity::class.java)
                startActivity(intent)
                true
            }

        }
        return super.onOptionsItemSelected(item)
    }

}